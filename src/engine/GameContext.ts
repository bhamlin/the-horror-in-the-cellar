import { writable } from "svelte/store";
import type { CombatStats, HPRange, Monster, Player } from "./Types";

export type GameState = {
    attacking: Monster,
    player: Player,
}

const initial_state = {
    attacking: {
        alive: false,
    } as Monster,
    player: {
        verb: 'Bonk',
        attack: {
            toHit: 0.5,
            damage: 2,
        } as CombatStats,
        autoAttack: {
            toHit: 1,
            damage: 0,
        } as CombatStats,
        xp: 0,
    } as Player,
}

export const State = writable(initial_state);
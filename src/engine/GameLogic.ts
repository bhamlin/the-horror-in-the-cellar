import { GameState, State } from "./GameContext";
import type { CombatStats, Emission, FullUpdate, HPRange } from "./Types";

class GameLogic {
    newMonster(state: GameState): FullUpdate {
        const emit = {} as Emission;
        const newState = { ...state } as GameState;
        const imageFile = 'goblin.svg';

        newState.attacking = {
            alive: true,
            name: 'Gobrin',
            hp: {
                max: 8,
                cur: 8
            },
            xpValue: 7,
        };

        emit.updateImage = { src: imageFile };

        return { emit, newState };
    }

    handleClick(action: string, state: GameState): Emission {
        let out = {} as Emission;
        let isAuto = false;
        switch (action) {
            case 'timer':
                // Things to do as auto attack timer
                isAuto = true;
            case 'attack':
                const attackOut = this.attackLifeCycle(state, isAuto);

                if ('stateUpdate' in attackOut.emit && attackOut.emit.stateUpdate) {
                    State.set(attackOut.newState);
                }
                out = attackOut.emit;
                break;
            case 'upgrade':
                // TODO: Upgrades
                let upgradeState = { ...state };
                const upgradeP = upgradeState.player;
                const Paa = upgradeP.autoAttack;
                if (upgradeP.xp >= 15) {
                    upgradeP.xp -= 15;
                    Paa.damage += 1;

                    State.set(upgradeState);
                }
                break;
            default:
                console.log('gamelocic::handleclick - Got action:', action);
        }
        return out;
    }

    attackLifeCycle(state: GameState, isAuto: boolean = false): FullUpdate {
        const emit = {} as Emission;
        const attackState = { ...state } as GameState;

        if (('alive' in state.attacking) && state.attacking.alive) {
            // console.log('handleclick', 'attack', 'auto:', isAuto);
            const attack = ((isAuto) ? (state.player.autoAttack) : (state.player.attack));
            // console.log('using attack:', attack.damage, attack.toHit);
            if (attack.damage > 0) {
                const newHp = this.attack(attack, state.attacking.hp);
                emit.stateUpdate = true;

                if (newHp.cur <= 0) {
                    attackState.attacking.alive = false;
                    attackState.player.xp += attackState.attacking.xpValue;
                    emit.itDied = true;
                    emit.updateImage = { src: 'ded.svg' };
                }

                attackState.attacking.hp = newHp;
            }
        }

        return { emit, newState: attackState };
    }

    attack(attack: CombatStats, hp: HPRange): HPRange {
        const hpOut = { ...hp };

        if (attack.damage > 0) {
            const roll = Math.random();

            if (roll < attack.toHit) {
                // Hit
                console.log('Hit', attack.damage);
                hpOut.cur = Math.max(0, hpOut.cur - attack.damage);
            } else {
                console.log('Miss');
            }
        }

        return hpOut;
    }
}

export const gameLogic = new GameLogic();
import type { GameState } from "./GameContext"

export type RangeValue = {
    cur: number,
    max: number,
}

export type HPRange = RangeValue & {

}

export type CombatStats = {
    toHit: number,
    damage: number,
}

export type Monster = {
    alive: boolean,
    name: string,
    hp: RangeValue,
    xpValue: number,
}

export type Player = {
    verb: string,
    attack: CombatStats,
    autoAttack: CombatStats,
    xp: number
}

export type Emission = Partial<{
    itDied: boolean,
    stateUpdate: boolean,
    updateImage: { src: string },
}>;

export type FullUpdate = {
    emit: Emission,
    newState: GameState,
}
